Công ty TNHH Sở hữu trí tuệ HAVIP là một Đại diện của Cục Sở hữu Trí tuệ. Cung cấp dịch vụ đăng ký nhãn hiệu, sáng chế, kiểu dáng công nghiệp.

Địa chỉ: Phòng 2002 tòa nhà Licogi 13 Tower, số 164 Khuất Duy Tiến, Nhân Chính, Thanh Xuân, Hà Nội

Điện thoại: 024 3552 5035

Mail: info@havip.com.vn

Website: https://havip.com.vn/

Hashtags: #havip